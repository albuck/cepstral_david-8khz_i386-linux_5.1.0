/* async_example.c: Asynchronous Text to Speech using Swift.
 *
 * Copyright (c) 2004-2006 Cepstral, LLC.  All rights reserved.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  CEPSTRAL, LLC DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * CEPSTRAL, LLC BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA
 * OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

/* This example program demonstrates the use of asynchronous API calls to
   pause and stop speech. */

#include <stdio.h>
#include <stdlib.h>
#include "swift.h"

#ifdef WIN32
    #include <windows.h>
    #include <conio.h>
#else
    #include <unistd.h>
    #include <sys/time.h>
    #include <termios.h>
    #include <errno.h>
    #include <string.h>

    #define getch() getchar() /* getch() is a builtin in DOS */ 
    struct termios old_settings, new_settings;
#endif // WIN32

const char usage[] = 
"Usage: async_example STRING [STRING...]\n\n";

void init_terminal();
void restore_terminal();
int key_down();
void nap(int msecs);

int main(int argc, char *argv[])
{
    swift_engine *engine;
    swift_port *port = NULL;
    swift_voice *voice;
    swift_background_t tts_stream;
    swift_result_t res;
    int i;

    if (1 == argc) {
        fprintf(stderr, usage);
        exit(1);
    }
    
    init_terminal();

    /* Open the Swift TTS Engine */
    if ( (engine = swift_engine_open(NULL)) == NULL) {
        fprintf(stderr, "Failed to open Swift Engine.");
        goto all_done;
    }
    /* Open a Swift Port through which to make TTS calls */
    if ( (port = swift_port_open(engine, NULL)) == NULL ) {
        fprintf(stderr, "Failed to open Swift Port.");
        goto all_done;
    }
    
    /* Find the first voice on the system */
    if ( (voice = swift_port_find_first_voice(port, NULL, NULL)) == NULL ) {
        fprintf(stderr, "Failed to find any voices!");
        goto all_done;
    }

    /* Set the voice found in the last step as the port's current voice */
    if ( SWIFT_FAILED(res = swift_port_set_voice(port, voice)) ) {
        const char *error_string = swift_strerror(res);
        fprintf(stderr, "%s", error_string);
        goto all_done;
    }


    printf("\nPress \"p\" to pause/unpause,\n\"s\" to stop,\nor \".\" to "
           "disable tick marks and wait for completion.\n\n");

    /* Synthesize each argument as a text string */
    for (i = 1; i < argc; ++i)
    {
        swift_port_speak_text(port, argv[i], 0, NULL, &tts_stream, NULL);
        printf("Speaking...");
        while( (SWIFT_STATUS_RUNNING == swift_port_status(port, tts_stream)) ||
               (SWIFT_STATUS_PAUSED == swift_port_status(port, tts_stream)) )
        {
            if (key_down()) {
                switch(getch())
                {
                    case 'p':
                        swift_port_pause(port, tts_stream, SWIFT_EVENT_NOW);
                        break;
                    case 's':
                        swift_port_stop(port, tts_stream, SWIFT_EVENT_NOW);
                        swift_port_wait(port, tts_stream);
                        break;
                    case '.':
                        swift_port_wait(port, tts_stream);
                }
            }
            nap(75);
    
            if (SWIFT_STATUS_RUNNING == swift_port_status(port, tts_stream))
                printf("."), fflush(stdout);
        }
        printf("\nDone.\n");
    }
    
all_done:
    /* Close the Swift Port and Engine */
    if (NULL != port) swift_port_close(port);
    if (NULL != engine) swift_engine_close(engine);

    restore_terminal();
    exit(0);
}

/* Platform specific helper functions */
#ifdef WIN32
void init_terminal()
{
    /* nothing to do */
}

void restore_terminal()
{
    /* nothing to do */
}
int key_down()
{
    return kbhit();
}

void nap(int msecs)
{
    Sleep(msecs);
}
#else
void init_terminal()
{
    tcgetattr(0, &old_settings);
    new_settings = old_settings;
    new_settings.c_lflag &= ~ICANON;
    new_settings.c_lflag &= ~ECHO;
    tcsetattr(0, TCSANOW, &new_settings);
}

void restore_terminal()
{
    tcsetattr(0, TCSANOW, &old_settings);
}

int key_down()
{
    fd_set set;
    struct timeval tv;
    
    memset(&tv, 0, sizeof(tv));
    FD_ZERO(&set);
    FD_SET(0, &set);
    select(1, &set, 0, 0, &tv);
    
    return FD_ISSET(0, &set);
}

void nap(int msecs)
{
    usleep(msecs * 1000);
};
#endif
