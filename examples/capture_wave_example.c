/* capture_wave_example.c: Demonstration of capturing audio from a Swift
 *                    callback.
 *
 * Copyright (c) 2004-2006 Cepstral, LLC.  All rights reserved.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  CEPSTRAL, LLC DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * CEPSTRAL, LLC BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA
 * OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

/* This example program synthesizes arguments from the command line,
   captures the audio in a callback, and saves it to file when synthesis is 
   completed. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "swift.h"

#ifndef TRUE
#define TRUE (1==1)
#endif
#ifndef FALSE
#define FALSE (1==0)
#endif

swift_result_t audio_callback(swift_event *event, swift_event_t type, void *udata);

const char usage[] =
"Usage: capture_wave_example FILENAME STRING [STRING...]\n\n";

int main(int argc, char *argv[])
{
    swift_engine *engine;
    swift_port *port = NULL;
    swift_voice *voice;
    swift_waveform *wave;
    unsigned int event_mask;
    int i;

    if (argc < 3) 
    {
        fprintf(stderr, usage);
        exit(0);
    }

    /* Open the Swift TTS Engine */
    if ( (engine = swift_engine_open(NULL)) == NULL ) 
    {
        fprintf(stderr, "Failed to open Swift Engine.\n");
        goto all_done;
    }

    wave = swift_waveform_new();
    
    /* Open a Swift Port through which to make TTS calls */
    if ( (port = swift_port_open(engine, NULL)) == NULL ) 
    {
        fprintf(stderr, "Failed to open Swift Port.\n");
        goto all_done;
    }

    /* Find the first voice on the system */
    if ((voice = swift_port_find_first_voice(port, NULL, NULL)) == NULL) 
    {
        fprintf(stderr, "Failed to find any voices!\n");
        goto all_done;
    }

    /* Set the voice found by find_first_voice() as the port's current voice */
    if ( SWIFT_FAILED(swift_port_set_voice(port, voice)) ) 
    {
        fprintf(stderr, "Failed to set voice.\n");
        goto all_done;
    }

    event_mask = SWIFT_EVENT_AUDIO; /* only audio events, please */

    /* Set audio_callback as a callback, with the output file as its param */
    swift_port_set_callback(port, &audio_callback, event_mask, wave);

    for (i = 2; i < argc; ++i) 
    {
        swift_result_t rv;
        rv = swift_port_speak_text(port, argv[i], 0, NULL, NULL, NULL);
        if (SWIFT_FAILED(rv)) 
        {
            fprintf(stderr, "Failed to speak prompt: %s\n", swift_strerror(rv));
            goto all_done;
        }
    }

    /* print the information about the wave */
    swift_waveform_print(wave);

    /* If you need the data converted here is where you do it:
     * swift_waveform_set_channels(wave, 2);   <-- make it stereo  
     * swift_waveform_resample(wave, 8000);    <-- convert to 8kHz  
     * swift_waveform_convert(wave, "ulaw");   <-- convert to uLaw   */

    /* save the wave to a file */
    swift_waveform_save(wave, argv[1], "riff");

all_done:
    /* Close the Swift Port and Engine */
    if (port != NULL) swift_port_close(port);
    if (engine != NULL) swift_engine_close(engine);

    return 0;
}

swift_result_t audio_callback(swift_event *event, swift_event_t type, void *udata)
{
    swift_waveform *wave = NULL;
    swift_event_t rv = SWIFT_SUCCESS;
    
    /* NOTE: if you modify this to run in async mode you will need to put a mutex
     * around the use of the wave module. */
    
    wave = udata;
    
    /* ask for the data to be put into the wave object.  In this example we 
     * want the data to be concatenated onto the wave object we are sending in */
    rv = swift_event_get_wave(event, &wave, TRUE);

    return rv;
}
