/* wave_example.c: Simple Text to Wave File using Swift.
 *
 * Copyright (c) 2004-2006 Cepstral, LLC.  All rights reserved.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  CEPSTRAL, LLC DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * CEPSTRAL, LLC BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA
 * OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

/* This example program shows how to perform text-to-speech, saving
 * the output as a wave file by setting swift parameters on the
 * synthesis call.
 */

#include <stdio.h>
#include <stdlib.h>
#include "swift.h"

const char usage[] = 
"Usage: wave_example <output file> STRING\n\n";

int main(int argc, char *argv[])
{
    swift_engine *engine;
    swift_port *port;
    swift_voice *voice;
    swift_params *params;
    swift_background_t tts_stream;

    if (3 != argc) {
        fprintf(stderr, usage);
        exit(1);
    }

    /* Open the Swift TTS Engine */
    if ( SWIFT_FAILED(engine = swift_engine_open(NULL)) ) {
        fprintf(stderr, "Failed to open Swift Engine.\n");
        goto all_done;
    }
    
    /* Create a Swift Parameters set through which to set the output
       file and format for saving the synthesized speech as a wave file. */    
    params = swift_params_new(NULL);
    swift_params_set_string(params, "audio/output-file", argv[1]);
    swift_params_set_string(params, "audio/output-format", "riff");
    
    /* Open a Swift Port through which to make TTS calls */
    if (NULL == (port = swift_port_open(engine, params))) {
        fprintf(stderr, "Failed to open Swift Port.\n");
        goto all_done;
    }
    
    /* Find the first voice on the system */
    if ( NULL == (voice = swift_port_find_first_voice(port, NULL, NULL)) ) {
        fprintf(stderr, "Failed to find any voices!\n");
        goto all_done;
    } 

    /* Set the voice found in the last step as the port's current voice */
    if ( SWIFT_FAILED(swift_port_set_voice(port, voice)) ) {
        fprintf(stderr, "Failed to set voice.\n");
        goto all_done;
    }

    /* Synthesize the text passed in */
    if ( SWIFT_FAILED(swift_port_speak_text(port, argv[2], 0, NULL, &tts_stream, NULL)) ) {
        fprintf(stderr, "Failed to speak.\n");
        goto all_done;
    }
    
all_done:
    /* Close the Swift Port and Engine */
    if (NULL != port) swift_port_close(port);
    if (NULL != engine) swift_engine_close(engine);
    exit(0);
}
