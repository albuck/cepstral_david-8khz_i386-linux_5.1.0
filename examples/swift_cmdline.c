/* swift_cmdline.c: A functional command-line TTS program.
 *
 * Copyright (c) 2004-2006 Cepstral, LLC.  All rights reserved.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  CEPSTRAL, LLC DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * CEPSTRAL, LLC BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA
 * OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

/* This example shows off a lot of what you can do with the Swift API.
 * Please see the declaration of of the const char usage[] string
 * below for an idea of what you can do.
 * Of particular interest is the ability to synthesize text files
 * containing SSML to wave files for later playback.  This can be
 * done via the following command:
 *     ./swift_cmdline -f <infile> -o <outfile> -m ssml
 * NOTE: "-m ssml" is not needed as it's the default for Swift.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "swift.h"

/* Simple linked list used for cmd line params */
struct arg_feat_val_pair_list {
    swift_val *feat;
    swift_val *val;
    struct arg_feat_val_pair_list *next;
};

typedef struct arg_feat_val_pair_list features;


features * get_args_list(int count, char *values[]);
int arg_feat_present(features *opts, char *feature);
const char *arg_feat_val(features *opts, char *feature);
void describe_params(void);
void list_voices(swift_port *port);
void list_engine_info();


const char usage[] = 
"Usage: swift [OPTIONS...] [STRINGS | FILES...]\n"
"       Help Arguments:  \n"
"            --help          Display This Message\n"
"            --params        Describe all available parameters\n"
"            --engine-info   Show info about the Swift Engine\n"
"            --voices        List all voices on system\n"
"       Attribute Arguments:  \n"
"            -f  <string>    Read input from file specified\n"
"                            Default is to speak all params not\n"
"                            denoted with a \"-\" or \"--\" option\n"
"            -o  <string>    Write output to file specified.\n"
"                            Default is to play to audio device.\n"
"            -d  <string>    Set voice from given directory.\n"
"            -e  <string>    Use text encoding specified.\n"
"            -n  <string>    Use voice named <string>.\n"
"                            Has precedence over -d switch\n"
"            -m  <string>    Mode: text or ssml.\n"
"            -X  <string>    Load SFX file specified\n"
;

int main(int argc, char *argv[])
{
    features *curr, *opts = NULL;
    swift_engine *engine;
    swift_port *port = NULL;
    swift_voice *voice;
    swift_background_t tts_stream;
    swift_params *params;

    /* Copy the command line arguments into the features list */
    opts = get_args_list(argc, argv);

    if ( (1 == argc) || (arg_feat_present(opts, "--help")) ) {
        printf("%s", usage);
        exit(1);
    }

    if (arg_feat_present(opts, "--params"))
        describe_params();

    if (arg_feat_present(opts, "--engine-info"))
        list_engine_info();


    /* Initialize the Swift Params structure */
    params = swift_params_new(NULL);

    if (arg_feat_present(opts, "-o")) {
        swift_params_set_string(params, "audio/output-file", arg_feat_val(opts, "-o"));
        swift_params_set_string(params, "audio/output-format", "riff");
    }

    /* Open the Swift TTS Engine */
    if ( (engine = swift_engine_open(params)) == NULL) {
        fprintf(stderr, "Failed to open Swift Engine.");
        goto all_done;
    }
    /* Open a Swift Port through which to make TTS calls */
    if ( (port = swift_port_open(engine, NULL)) == NULL) {
        fprintf(stderr, "Failed to open Swift Port.");
        goto all_done;
    }

    if (arg_feat_present(opts, "-m")) {
        const char *mode = arg_feat_val(opts, "-m");
        /* Set the mode to plain-text if they ask for it, otherwise default to ssml */
        if(!strcmp(mode, "text"))
            swift_port_set_param(port, "tts/content-type", swift_val_string("text/plain"), SWIFT_ASYNC_NONE);
        else
            swift_port_set_param(port, "tts/content-type", swift_val_string("text/ssml"), SWIFT_ASYNC_NONE);
    }

    /* If the user asked to see all voices on the system, show them */
    if (arg_feat_present(opts, "--voices")) {
        list_voices(port);
    }

    /* If the user asked to load an SFX file, load it */
    if (arg_feat_present(opts, "-X")) {
        swift_port_load_sfx(port, arg_feat_val(opts, "-X"));
    }
    
    /* If the user asked for a voice by name, load it. */
    if (arg_feat_present(opts, "-n")) {
        if ( SWIFT_FAILED(swift_port_set_voice_by_name(port, arg_feat_val(opts, "-n"))) ) {
            /* NOTE: This function calls swift_port_find_first_voice internally.
                     It only fails if it cannot find the voice requested AND
                     it cannot load the voice returned by find_first_voice. */
            fprintf(stderr, "Failed to load requested voice");
            goto all_done;
        }
    }
    /* Else if they asked for a voice from a voice directory, load it */
    else if (arg_feat_present(opts, "-d")) {
        if ( SWIFT_FAILED(swift_port_set_voice_from_dir(port, arg_feat_val(opts, "-d"))) ) {
            fprintf(stderr, "Failed to load requested voice");
            goto all_done;
        }
    }
    /* Else just use the first voice found */
    else {
        if ((voice = swift_port_find_first_voice(port, NULL, NULL)) == NULL) {
            fprintf(stderr, "Failed to find any voices!");
            goto all_done;
        }
        if ( SWIFT_FAILED(swift_port_set_voice(port, voice)) ) {
            fprintf(stderr, "Failed to set voice.");
            goto all_done;
        }
    }

    /* If the user asked for a files to be spoken, speak it */
    if (arg_feat_present(opts, "-f"))
        swift_port_speak_file(port, arg_feat_val(opts, "-f"), arg_feat_val(opts, "-e"), NULL, NULL);

    /* Synthesize each argument as a text string */
    for (curr = opts; curr; curr = curr->next) {
        if (!strcmp(swift_val_get_string(curr->feat), "text")) {
            const char *text = swift_val_get_string(curr->val);
            swift_port_speak_text(port, text, 0, arg_feat_val(opts, "-e"), &tts_stream, NULL); 
        }
    }
    
all_done:
    /* Close the Swift Port and Engine */
    if (NULL != port) swift_port_close(port);
    if (NULL != engine) swift_engine_close(engine);
    exit(0);
}


const char *arg_feat_val(features *opts, char *feature) {
    features *curr;
    for (curr = opts; curr; curr = curr->next) {
        if (!strcmp(swift_val_get_string(curr->feat), feature))
            return swift_val_get_string(curr->val);
    }
    return NULL;
}


int arg_feat_present(features *opts, char *feature) {
    features *curr;
    for (curr = opts; curr; curr = curr->next) {
        if (!strcmp(swift_val_get_string(curr->feat), feature))
            return 1;
    }
    return 0;
}

features * get_args_list(int count, char *values[])
{
    int i;
    features *curr, *prev, *head;
    head = curr = prev = NULL;
    for (i = 1; i<count; i++) {
        if ('-' == values[i][0]) {
            curr = (features *)malloc(sizeof(features));
            curr->next = NULL;
            curr->feat = swift_val_string(values[i]);
            /* --xxxx params don't have a value */
            if ('-' == values[i][1])
                curr->val = NULL;
            else {
                curr->val = swift_val_string(values[i+1]);
                i++;
            }
            if (NULL == head)
                head = curr; 
            else 
                prev->next = curr;
            prev = curr;
        }
        else { /* Text to synth */
            curr = (features *)malloc(sizeof(features));
            curr->next = NULL;
            curr->feat = swift_val_string("text");
            curr->val = swift_val_string(values[i]);
            if (NULL == head)
                head = curr; 
            else 
                prev->next = curr;
            prev = curr;
        }
    }
    return head;
}

void describe_params(void)
{
    const swift_param_desc *desc;

    /* Describe every parameter in the public list of param descriptors */
    for (desc = swift_param_descriptors; desc->name; ++desc) {
        if (desc->type == SWIFT_PARAM_NONE)
            continue;
        if (desc->undocumented)
            continue;
        printf("%s: ", desc->name);
        switch (desc->type) {
        case SWIFT_PARAM_NONE:
            break;
        case SWIFT_PARAM_FLAG:
            printf("Flag (1/0)\n");
            break;
        case SWIFT_PARAM_INT:
            printf("Integer\n");
            break;
        case SWIFT_PARAM_FLOAT:
            printf("Floating-point\n");
            break;
        case SWIFT_PARAM_STRING:
            printf("String\n");
            break;
        case SWIFT_PARAM_ENUM:
        {
            const char **eval;
            printf("One of [ ");
            for (eval = desc->enum_vals; eval[0] && eval[1]; ++eval)
                printf("%s, ", eval[0]);
            printf("%s ]\n", eval[0]);
            break;
        }
        }
        printf("\t%s\n", desc->help);
    }
}

void list_engine_info()
{
    printf("Engine Name: %s\n", swift_engine_name);
    printf("Engine Version: %s\n", swift_version);
    printf("Engine Build Date: %s\n", swift_date);
    printf("Your Platform: %s\n\n", swift_platform);
}


void list_voices(swift_port *port) {
    swift_voice *voice;
    const char *license_status;
    
    /* Find the first voice on the system */
    if ((voice = swift_port_find_first_voice(port, NULL, NULL)) == NULL) {
        fprintf(stderr, "Failed to find any voices!");
        return;
    }
    /* Go through all of the voices on the system and print some info about each */
    printf("Available voices:\n");
    for (; voice != NULL; voice = swift_port_find_next_voice(port))
    {
        if (swift_voice_get_attribute(voice, "license/key"))
             license_status = "licensed";
        else
             license_status = "unlicensed";
        printf("%s: %s, age %s, %s, %sHz, %s\n",
               swift_voice_get_attribute(voice, "name"),
               swift_voice_get_attribute(voice, "speaker/gender"),
               swift_voice_get_attribute(voice, "speaker/age"),
               swift_voice_get_attribute(voice, "language/name"),
               swift_voice_get_attribute(voice, "sample-rate"),
               license_status);
    }
}
