/* event_example.c: Demonstration of setting a Swift callback
 *                  function to capture TTS Engine events.
 *
 * Copyright (c) 2004-2006 Cepstral, LLC.  All rights reserved.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  CEPSTRAL, LLC DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * CEPSTRAL, LLC BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA
 * OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

/* This example program synthesizes each argument passed to it as a
   text string.  It registers a synthesis event callback which prints
   out a description of each event received. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "swift.h"

swift_result_t handle_events(swift_event *event, swift_event_t type, void *udata);

const char usage[] = 
"Usage: event_example STRING [STRING...]\n\n";

static int event_count = 0;

int main(int argc, char *argv[])
{
    swift_engine *engine;
    swift_port *port = NULL;
    swift_voice *voice;
    swift_background_t tts_stream;
    int event_mask, i;

    if (1 == argc) {
        fprintf(stderr, usage);
        exit(0);
    }

    /* Open the Swift TTS Engine */
    if ( (engine = swift_engine_open(NULL)) == NULL ) {
        fprintf(stderr, "Failed to open Swift Engine.");
        goto all_done;
    }
    /* Open a Swift Port through which to make TTS calls */
    if ( (port = swift_port_open(engine, NULL)) == NULL ) {
        fprintf(stderr, "Failed to open Swift Port.");
        goto all_done;
    }
    
    /* Find the first voice on the system */
    if ((voice = swift_port_find_first_voice(port, NULL, NULL)) == NULL) {
        fprintf(stderr, "Failed to find any voices!");
        goto all_done;
    }

    /* Set the voice found in the last step as the port's current voice */
    if ( SWIFT_FAILED(swift_port_set_voice(port, voice)) ) {
        fprintf(stderr, "Failed to set voice.");
        goto all_done;
    }

    /* Set a callback function to handle the events sent back from Swift */
    /* Other event masks are defined in swift_defs.h */
    event_mask = ~SWIFT_EVENT_AUDIO;
    swift_port_set_callback(port, &handle_events, event_mask, NULL);

    /* Synthesize each argument as a text string */
    printf("\n\n    |             | Start  | End    | Text Pos |\n");
    printf(    "No. | Type        | (Sec.) | (Sec.) | (Chars)  | Value\n");
    printf(    "----|-------------|--------|--------|----------|---------------------------\n");
    for (i = 1; i < argc; ++i) {
        swift_port_speak_text(port, argv[i], 0, NULL, &tts_stream, NULL); 
    }
    
all_done:
    /* Close the Swift Port and Engine */
    if (NULL != port) swift_port_close(port);
    if (NULL != engine) swift_engine_close(engine);
    exit(0);
}

swift_result_t handle_events(swift_event *event, swift_event_t type, void *udata)
{
    float time_start, time_len;
    int   text_start, text_len, i;
    const char *type_name = "";
    char *text = "";
    char *trunc_text = NULL;
    char evt_count[10];

    event_count++;
    sprintf(evt_count,"%d",event_count);

    /* Get the string name of the event type */
    type_name = swift_event_type_get_name(type);
    /* Get the event times */
    swift_event_get_times(event, &time_start, &time_len);
    /* Get the event text */
    swift_event_get_text(event, &text);

    if ("" == text)
        text = "N/A";
    if (26 < strlen(text))
    {
        trunc_text = (char *)malloc(27);
        strncpy(trunc_text, text, 23);
        trunc_text[23] = '.';
        trunc_text[24] = '.';
        trunc_text[25] = '.';
        trunc_text[26] = 0;
    }

    printf("%s", evt_count);
    for (i = strlen(evt_count); i<4; i++) printf(" ");
    printf("| %s", type_name);
    for (i = strlen(type_name); i<12; i++) printf(" ");
    printf("| %0.4f | %0.4f ", time_start, time_start + time_len);



    /* Get the event text positions, if any */
    if (!SWIFT_FAILED(swift_event_get_textpos(event, &text_start, &text_len))) {
        char text_start_str[12];
        char text_end_str[12];
        sprintf(text_start_str, "%d", text_start);
        sprintf(text_end_str, "%d", text_start + text_len);
        printf("| %s-%s ", text_start_str, text_end_str);
        for (i = (strlen(text_start_str)+strlen(text_end_str)+1); i<8; i++)
            printf(" ");
    }
    else
        printf("| N/A      ");


    if (trunc_text)
        printf("| %s\n", trunc_text);
    else
        printf("| %s\n", text);

    fflush(stdout);
    if (trunc_text) free(trunc_text);

    return SWIFT_SUCCESS;
}

