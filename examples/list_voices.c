/* list_voices.c: List info about installed Swift Voices
 *
 * Copyright (c) 2004-2006 Cepstral, LLC.  All rights reserved.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  CEPSTRAL, LLC DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * CEPSTRAL, LLC BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA
 * OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

/* This example program lists all installed voices on the system, 
 * their gender, age, language, sampling rate, and license status.
 * Or, the user can enter attributes on the command line to
 * limit the voices returned. 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "swift.h"

void list_voices(swift_port *port, const char *required, const char *optional);

const char usage[] = 
"Usage: list_voices [required] [optional]\n"
"       required: <string> voices must have this attribute\n"
"       optional: <string> voices may have this attribute\n"
"Available attributes:\n"
"       - id\n"
"       - name\n"
"       - path\n"
"       - version\n"
"       - buildstamp\n"
"       - sample-rate\n"
"       - license/key\n"
"       - language/tag\n"
"       - language/name\n"
"       - language/version\n"
"       - lexicon/name\n"
"       - lexicon/version\n"
"       - speaker/name\n"
"       - speaker/gender\n"
"       - speaker/age\n\n";

int main(int argc, char *argv[]) {
    swift_engine *engine;
    swift_port *port = NULL;
    char *required = NULL;
    char *optional = NULL;

    /* Print the usage message */
    printf("%s", usage);

    if (3 < argc)
        exit(1);
    if (3 == argc) {
        optional = (char *)malloc(strlen(argv[2])+1);
        strcpy(optional, argv[2]);
    }
    if (2 <= argc) {
        required = (char *)malloc(strlen(argv[1])+1);
        strcpy(required, argv[1]);
    }

    /* Open the Swift TTS Engine */
    if ( (engine = swift_engine_open(NULL)) == NULL ) {
        fprintf(stderr, "Failed to open Swift Engine\n");
        goto all_done;
    }
    /* Open a Swift Port through which to find voices */
    if ( (port = swift_port_open(engine, NULL)) == NULL ) {
        fprintf(stderr, "Failed to open Swift Port\n");
        goto all_done;
    }

    list_voices(port, required, optional);

all_done:
    free(required);
    free(optional);
    
    /* Close the Swift Port and Swift Engine */
    if (NULL != port) swift_port_close(port);
    if (NULL != engine) swift_engine_close(engine);
    exit(0);
}


void list_voices(swift_port *port, const char *required, const char *optional) {
    swift_voice *voice;
    const char *license_status;
    
    /* Find the first voice on the system */
    if ( (voice = swift_port_find_first_voice(port, required, optional)) == NULL) {
        fprintf(stderr, "Failed to find any voices!\n");
        return;
    }
    /* Go through all of the voices on the system and print some info about each */
    printf("Available voices:\n");
    for (; voice; voice = swift_port_find_next_voice(port))
    {
        if (swift_voice_get_attribute(voice, "license/key"))
             license_status = "licensed";
        else
             license_status = "unlicensed";
        printf("%s: %s, age %s, %s, %sHz, %s\n",
               swift_voice_get_attribute(voice, "name"),
               swift_voice_get_attribute(voice, "speaker/gender"),
               swift_voice_get_attribute(voice, "speaker/age"),
               swift_voice_get_attribute(voice, "language/name"),
               swift_voice_get_attribute(voice, "sample-rate"),
               license_status);
    }
}
