/* capture_example.c: Demonstration of capturing audio from a Swift
 *                    callback.
 *
 * Copyright (c) 2004-2006 Cepstral, LLC.  All rights reserved.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  CEPSTRAL, LLC DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * CEPSTRAL, LLC BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA
 * OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

/* This example program synthesizes arguments from the command line,
   captures the raw audio in a callback, and writes it to a file. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "swift.h"

swift_result_t write_audio(swift_event *event, swift_event_t type, void *udata);

const char usage[] =
"Usage: capture_example FILENAME STRING [STRING...]\n\n";

int main(int argc, char *argv[])
{
    swift_engine *engine;
    swift_port *port = NULL;
    swift_voice *voice;
    swift_params *params;
    FILE* audio_out;
    unsigned int event_mask;
    int i;

    if (argc < 3) {
        fprintf(stderr, usage);
        exit(0);
    }

    /* Open the Swift TTS Engine */
    if ( (engine = swift_engine_open(NULL)) == NULL ) {
        fprintf(stderr, "Failed to open Swift Engine.\n");
        goto all_done;
    }

    /* Set up the encoding & sampling-rate parameters
     * (Change them to match your system.)
     * Note that if you actually want pcm16, 16kHz, under Desktop
     * Windows, Linux, Solaris, or OS X, or if you want pcm16, 
     * 11kHz under Windows CE, or ARM Linux, you needn't set these
     * parameters as they are the default. */
    params = swift_params_new(NULL);
    swift_params_set_string(params, "audio/encoding", "pcm16");
    swift_params_set_int(params, "audio/sampling-rate", 16000);

    /* Open a Swift Port through which to make TTS calls */
    if ( (port = swift_port_open(engine, params)) == NULL ) {
        fprintf(stderr, "Failed to open Swift Port.\n");
        goto all_done;
    }

    /* Find the first voice on the system */
    if ((voice = swift_port_find_first_voice(port, NULL, NULL)) == NULL) {
        fprintf(stderr, "Failed to find any voices!\n");
        goto all_done;
    }

    /* Set the voice found by find_first_voice() as the port's current voice */
    if ( SWIFT_FAILED(swift_port_set_voice(port, voice)) ) {
        fprintf(stderr, "Failed to set voice.\n");
        goto all_done;
    }

    event_mask = SWIFT_EVENT_AUDIO; /* only audio events, please */
    audio_out = fopen(argv[1], "wb");
    /* Set write_audio as a callback, with the output file as its param */
    swift_port_set_callback(port, &write_audio, event_mask, audio_out);

    for (i = 2; i < argc; ++i) {
    swift_result_t rv;
        if (SWIFT_FAILED(rv = swift_port_speak_text(port, argv[i], 0,
                                               NULL, NULL, NULL))) {
            fprintf(stderr, "Failed to speak prompt: %s\n", swift_strerror(rv));
            goto all_done;
        }
    }

    fclose(audio_out);

all_done:
    /* Close the Swift Port and Engine */
    if (port != NULL) swift_port_close(port);
    if (engine != NULL) swift_engine_close(engine);

    return 0;
}

swift_result_t write_audio(swift_event *event, swift_event_t type, void *udata)
{
    void *buf;
    int len;
    FILE *audio_out = udata;
    swift_event_t rv = SWIFT_SUCCESS;
    
    rv = swift_event_get_audio(event, &buf, &len);
    if (!SWIFT_FAILED(rv))
        fwrite(buf, 1, len, audio_out);

    return rv;
}
